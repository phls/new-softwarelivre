/* Google Analytics */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-26286704-1']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* Redirecionar perfil /ASL -> /asl */
jQuery(function($) {
  if (document.location.pathname == '/ASL') {
    $('#not-found').html('<h1>redirecionando...</h1>');
    document.location.href = '/asl';
  }
});

(function($) {
  var language = $('html').attr('lang');
  if (language == 'pt') {
    $("#submenu-about span").html('Sobre');
  }
})(jQuery);
